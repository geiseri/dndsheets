# DND Sheets for my games

To setup:

```bash
conda env update -f environment.yml 
```

```
To generate:

```bash
conda activate dndsheets
makesheets -F .
```

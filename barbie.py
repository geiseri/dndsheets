"""This file describes the heroic adventurer Barbarian1.

It's used primarily for saving characters from create-character,
where there will be many missing sections.

Modify this file as you level up and then re-generate the character
sheet by running ``makesheets`` from the command line.

"""
from dungeonsheets import mechanics, import_homebrew, background
_campaign = import_homebrew("homebrew.py")


dungeonsheets_version = "0.9.4"

name = "Barbie"
player_name = "Ian"

classes = ['Barbarian']
levels = [7]
subclasses = ["Path of the Berserker"]
background = _campaign.Haunted
race = _campaign.LongtoothShifter
alignment = "Chaotic Neutral"

xp = 0
hp_max = 82
inspiration = 0

# Ability Scores
strength = 16 + _campaign.LongtoothShifter.strength_bonus
dexterity = 17 + _campaign.LongtoothShifter.dexterity_bonus
constitution = 18 + _campaign.LongtoothShifter.constitution_bonus
intelligence = 18
wisdom = 14
charisma = 14

# Select what skills you're proficient with
skill_proficiencies = ('athletics', 'nature', 'investigation', 'arcana')

# Any skills you have "expertise" (Bard/Rogue) in
skill_expertise = ()

features = ('Tavern Brawler')

# If selecting among multiple feature options: ex Fighting Style
# feature_choices = ('Archery',)
feature_choices = ()

# Weapons/other proficiencies not given by class/race/background
weapon_proficiencies = ()
proficiencies_text = ()

# Proficiencies and languages
languages = """Common,Sylvan"""

# Inventory
# TODO: Get yourself some money
cp = 0
sp = 0
ep = 0
gp = 0
pp = 0


class MeatArmor(mechanics.Armor, mechanics.MagicItem):
    """My body is my armor."""
    name = "Meat Armor"
    base_armor_class = 16


# TODO: Put your equipped weapons and armor here
weapons = ('+1 maul',)
magic_items = ('Ring of Free Action')  # Example: ('ring of protection',)
armor = MeatArmor
shield = ""

equipment = """"""

attacks_and_spellcasting = """"""

# List of known spells
# Example: spells_prepared = ('magic missile', 'mage armor')
spells_prepared = ()  # Todo: Learn some spells

# Which spells have not been prepared
__spells_unprepared = ()

# all spells known
spells = spells_prepared + __spells_unprepared

# Backstory
# Describe your backstory here
personality_traits = """TODO: problematic"""

ideals = """Freedom"""

bonds = """#5"""

flaws = """TODO: many"""

features_and_traits = """TODO"""

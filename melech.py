"""This file describes the heroic adventurer Barbarian1.

It's used primarily for saving characters from create-character,
where there will be many missing sections.

Modify this file as you level up and then re-generate the character
sheet by running ``makesheets`` from the command line.

"""
from dungeonsheets import mechanics, import_homebrew, background
_campaign = import_homebrew("homebrew.py")


dungeonsheets_version = "0.9.4"

name = "Melech"
player_name = "Ian"

classes = ['Cleric']
levels = [5]
subclasses = ["Nature Domain"]
background = "Far Traveler"
race = "Tiefling"
alignment = "Chaotic Neutral"

xp = 'NA'
hp_max = 43
inspiration = 0

# Ability Scores
strength = 14
dexterity = 8
constitution = 18
intelligence = 18
wisdom = 18
charisma = 18

# Select what skills you're proficient with
skill_proficiencies = ('animal handling', 'history', 'insight',
                       'nature', 'perception', 'persuasion', 'survival')

# Any skills you have "expertise" (Bard/Rogue) in
skill_expertise = ()

features = ()

# If selecting among multiple feature options: ex Fighting Style
# feature_choices = ('Archery',)
feature_choices = ()

# Weapons/other proficiencies not given by class/race/background
weapon_proficiencies = ('Simple Weapons', "Light Armor",
                        "Medium Armor", "Heavy Armor")
proficiencies_text = ()

# Proficiencies and languages
languages = "Common, Sylvan"

# Inventory
# TODO: Get yourself some money
cp = 0
sp = 0
ep = 0
gp = 0
pp = 0


# TODO: Put your equipped weapons and armor here
weapons = ('Mace', 'dagger')
magic_items = ('Immovable Rod')  # Example: ('ring of protection',)
armor = "Chain Mail"
shield = "Shield"

equipment = """"""

attacks_and_spellcasting = """"""

# List of known spells
# Example: spells_prepared = ('magic missile', 'mage armor')
spells_prepared = (
    "Create Bonfire",
    "Guidance",
    "Infestation",
    "Poison Spray",
    "Spare the Dying",
    "Thaumaturgy",
    "Word of Radiance",
)  # Todo: Learn some spells

# Which spells have not been prepared
__spells_unprepared = (
    "Aid",
    "Animal Friendship",
    "Animate Dead",
    "Augury",
    "Bane",
    "Barkskin",
    "Beacon of Hope",
    "Bestow Curse",
    "Bless",
    "Blindness/Deafness",
    "Borrowed Knowledge",
    "Calm Emotions",
    "Ceremony",
    "Clairvoyance",
    "Command",
    "Continual Flame",
    "Create Food and Water",
    "Create or Destroy Water",
    "Cure Wounds",
    "Daylight",
    "Detect Evil and Good",
    "Detect Magic",
    "Detect Poison and Disease",
    "Dispel Magic",
    "Enhance Ability",
    "Fast Friends",
    "Feign Death",
    "Find Traps",
    "Glyph of Warding",
    "Guiding Bolt",
    "Healing Word",
    "Hellish Rebuke",
    "Hold Person",
    "Incite Greed",
    "Inflict Wounds",
    "Lesser Restoration",
    "Life Transference",
    "Locate Object",
    "Magic Circle",
    "Mass Healing Word",
    "Meld into Stone",
    "Motivational Speech",
    "Plant Growth",
    "Prayer of Healing",
    "Protection from Energy",
    "Protection from Evil and Good",
    "Protection from Poison",
    "Purify Food and Drink",
    "Remove Curse",
    "Revivify",
    "Sanctuary",
    "Sending",
    "Shield of Faith",
    "Silence",
    "Spike Growth",
    "Spirit Guardians",
    "Spirit Shroud",
    "Thunderwave",
    "Tongues",
    "Warding Bond",
    "Water Walk",
    "Wind Wall",
    "Zone of Truth",

)

# all spells known
spells = spells_prepared + __spells_unprepared

# Backstory
# Describe your backstory here
personality_traits = """TODO: problematic"""

ideals = """Freedom"""

bonds = """#5"""

flaws = """TODO: many"""

features_and_traits = """TODO"""
